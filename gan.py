import argparse
from data.base_dataset import BaseDataset, get_transform
from data.image_folder import make_dataset
from PIL import Image
from options.test_options import TestOptions
from models import create_model
import sys
import cv2
import torchvision.transforms as transforms
import torch
from util.util import tensor2im

class image_transformer:
    def __init__(self, arguments):
        sys.argv = sys.argv + arguments
        self.opt = TestOptions().parse()  # get test options
        # hard-code some parameters for test
        self.opt.num_threads = 0   # test code only supports num_threads = 1
        self.opt.batch_size = 1    # test code only supports batch_size = 1
        self.opt.serial_batches = True  # disable data shuffling; comment this line if results on randomly chosen images are needed.
        self.opt.no_flip = True    # no flip; comment this line if results on flipped images are needed.
        self.opt.display_id = -1   # no visdom display; the test code saves the results to a HTML file.

        transform_list = [transforms.Resize([self.opt.load_size, self.opt.load_size], Image.BICUBIC), 
                          transforms.ToTensor(), 
                          transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))]
        self.transform = transforms.Compose(transform_list)
        self.model = create_model(self.opt)
        self.model.setup(self.opt)


    def transformer(self, obs):
        pil_obs = Image.fromarray(obs)
        transformed_obs = self.transform(pil_obs)
        shape = [1] + [transformed_obs.shape[0], transformed_obs.shape[1], transformed_obs.shape[2]]
        transformed_obs = transformed_obs.reshape(shape)
        self.model.set_input({'A': transformed_obs, 'A_paths': 'None'})
        self.model.test()
        visuals = self.model.get_current_visuals()
        fake_image_tensor = visuals['fake']
        fake_image = tensor2im(fake_image_tensor)
        return fake_image
