import matplotlib.pyplot as plt
import numpy as np
import argparse
import ffmpeg
import shutil
import glob
import sys
import cv2
import os

def combine_training_outputs(input_path, output_path):
    """
    input:
        input_path: path to images
        output_path: path to save results
    return:
    """
    if input_path[-1] != '/': input_path += '/'
    image_paths = sorted(glob.glob(input_path + '*'))
    images = []
    for image_path in image_paths:
        image = cv2.imread(image_path)
        images.append(image)

    if not os.path.exists(output_path):
        os.makedirs(output_path)
    for i in range(int(len(images)/8)):
        # for j in range(8):
        #     print(image_paths[8*i + j])
        cv2.imwrite(output_path + '{}.png'.format(i).zfill(5), np.append(np.concatenate((images[8*i + 4],images[8*i + 1], images[8*i + 6], images[8*i + 3]), axis=1),
                                                  np.concatenate((images[8*i + 5], images[8*i + 0], images[8*i + 7], images[8*i + 2]), axis=1), axis=0))


def process_test_outputs(input_path, output_path):
    """
    input:
        input_path: path to images
        output_path: path to save results
    return:
    """
    if input_path[-1] != '/': input_path += '/'
    image_paths = sorted(glob.glob(input_path + '*'))

    if output_path[-1] != '/': input_path += '/'
    real_a_path = output_path + 'real_a/'
    fake_b_path = output_path + 'fake_b/'
    for directory in [real_a_path, fake_b_path]:
        if not os.path.exists(directory):
            os.makedirs(directory)

    for image_path in image_paths:
        if image_path[-10:] == 'real_A.png':
            image_name = image_pat/h.split('/')[-1]
            shutil.copy(image_path, real_a_path + image_name)
        elif image_path[-10:] == 'fake_B.png':
            image_name = image_path.split('/')[-1]
            shutil.copy(image_path, fake_b_path + image_name)


def make_video(input_path:str, output_path:str, filename:str=None):
    """
    input:
        input_path: path to images
        output_path: path to output video
        filename: filename for video
    return:
    """
    if input_path[-1] != '/': input_path += '/'
    if output_path[-1] != '/': output_path += '/'
    output_path += filename + '.mp4' if filename is not None else 'movie.mp4'
    (
    ffmpeg
    .input(input_path + '*.png', pattern_type='glob', framerate=55)
    .output(output_path, **{'qscale:v': 1})
    .run()
    )


def concat_images(image_ones:list, image_twos:list) -> list:
    """
    input:
        image_ones: list containing images to be concatenated on the left
        image_twos: list containing images to be concatenated on the right
    return:
        output_images: list of grayscaled images
    """
    image_sets = [image_ones, image_twos]
    output_images = []

    for i in range(len(image_sets[1])):
        image_one = image_sets[0][i]
        image_two = image_sets[1][i]
        output_images.append(cv2.hconcat([image_one, image_two]))

    return output_images

def main():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--input_one_path', default=None, type=str)
    parser.add_argument('--input_two_path', default=None, type=str)
    parser.add_argument('--output_path', default=None, type=str)
    parser.add_argument('--make_video', default=False, type=bool)
    parser.add_argument('--concat_images', default=False, type=bool)
    parser.add_argument('--process_test_outputs', default=False, type=bool)
    parser.add_argument('--combine_training_outputs', default=False, type=bool)
    args = parser.parse_args()

    if args.input_one_path is None or not os.path.exists(args.input_one_path):
        print('Provide valid image directory path')
        sys.exit()
    elif args.concat_images == True and (args.input_two_path is None or not os.path.exists(args.input_two_path)):
        print('Provide two input paths for images to be concatenated')
        sys.exit()

    if args.input_one_path[-1] != '/':
        args.input_one_path += '/'
    if args.output_path is None:
        output_path = args.input_one_path + 'results/'
    elif args.output_path[-1] != '/':
        output_path += '/'
    if not os.path.exists(output_path):
        os.mkdir(output_path)

    image_one_paths = sorted(glob.glob(args.input_one_path + '*'))
    image_ones = [cv2.imread(image_path) for image_path in image_paths]

    if args.concat_images:
        if args.input_two_path[-1] != '/':
            args.input_two_path += '/'
        image_two_paths = sorted(glob.glob(args.input_two_path + '*'))
        image_twos = [cv2.imread(image_path) for image_path in image_two_paths]
        output = concat_images(image_ones, image_twos)

    if args.make_video:
        make_video(args.input_one_path, args.output_path)

    if args.process_test_outputs:
        process_test_outputs(args.input_path, args.output_path)

    if args.combine_training_outputs:
        combine_training_outputs(args.input_one_path, args.output_path)

if __name__ == main():
    main()