import matplotlib.pyplot as plt
import datetime
import argparse
import random
import retro
import cv2
import os

'''
Mortal Kombat:
name: MortalKombat3-Genesis
states: kabal_jax_lvl1_medium, kabal_kano_lvl1_medium, kabal_liukang_lvl1_medium, kabal_nightwolf_lvl1_medium,
        kabal_sindel_lvl1_medium, kabal_sonya_lvl1_medium

Street Fighter:
name: StreetFighterIISpecialChampionEdition-Genesis
states: vega_chunli, vega_ehonda, vega_guile, vega_ken, vega_ryu, vega_zangief
'''

def main():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--game', default='MortalKombat3-Genesis', type=str)
    parser.add_argument('--state', default='kabal_jax_lvl1_medium', type=str)
    parser.add_argument('--output_path', default='./mortal_kombat_images/', type=str)
    parser.add_argument('--sample_interval', default=200, type=int)
    parser.add_argument('--num_images', default=2000, type=int)
    args = parser.parse_args()

    if args.output_path[-1] != '/':
        args.output_path += '/'
    args.output_path = args.output_path + state + '/'
    if not os.path.exists(args.output_path):
        os.makedirs(args.output_path)

    num_collected = 0
    while num_collected < args.parse_args():
        env = retro.make(game=args.game, state=args.state)
        obs = env.reset()

        count = 0
        lost_match = False
        while not lost_match:
            obs, rew, done, info = env.step(env.action_space.sample())  # take a random action
            # env.render()
            if count % args.sample_interval == 0:
                filename = args.output_path + datetime.datetime.now().strftime('%Y%m%d-%H%M%S.%f') + '.png'
                plt.imsave(filename, obs)
                num_collected += 1

            if info['enemy_matches_won'] != 0 or info['matches_won'] != 0:
                lost_match = True
            count += 1
        env.close()


if __name__ == "__main__":
    main()