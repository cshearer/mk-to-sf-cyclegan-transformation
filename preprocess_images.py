import matplotlib.pyplot as plt
import numpy as np
import argparse
import glob
import sys
import cv2
import os


def split_subway_street(images:list, threshold:float) -> (list, list):
    """
    input:
        images: list containing images to be processed, images should be in BGR color format
        threshold: average red pixel value to threshold images as whiteout
    return:
        subway_images: list of all subway images
        street_images: list of all street images
    """
    subway_images = []
    street_images = []

    for image in images:
        average_red = np.sum(image[:, :, 2]) / image[:, :, 2].size
        if average_red > threshold:
            subway_images.append(image)
        else:
            street_images.append(image)
    return subway_images, street_images


def filter_whiteout(images:list, threshold:float) -> (list, list):
    """
    input:
        images: list containing images to be processed, images should be in BGR color format
        threshold: average blue pixel value to threshold images as whiteout
    return:
        valid_images: list of all non-whiteout images
        whiteout_images: list of all whiteout images
    """
    valid_images = []
    whiteout_images = []
    for image in images:
        average_blue = np.sum(image[:, :, 1]) / image[:, :, 1].size
        if average_blue > threshold:
            whiteout_images.append(image)
        else:
            valid_images.append(image)
    return valid_images, whiteout_images


def resize_images(images:list, dimensions:[int, int]) -> list:
    """
    input:
        images: list containing images to be resized
        dimensions: desired dimensions of output images
    return:
        resized_images: list of resized images
    """
    resized_images = []
    for image in images:
        resized_images.append(cv2.resize(image, dimensions))
    return resized_images


def grayscale_images(images:list) -> list:
    """
    input:
        images: list containing images to be grayscaled
    return:
        grayscaled_images: list of grayscaled images
    """
    grayscaled_images = []
    for image in images:
        grayscaled_images.append(cv2.cvtColor(image, cv2.COLOR_BGR2GRAY))
    return grayscaled_images


# white_out_image_0 blue level: 74.46495535714286
# white_out_image_0 green level: 78.76378348214286
# white_out_image_0 red level: 111.30435267857143
# white_out_image_4 blue level: 109.92957589285714
# white_out_image_4 green level: 111.81495535714286
# white_out_image_4 red level: 143.43404017857142
# Average blue per pixel in underground: 43.894057145198204
# Average green per pixel in underground: 44.72657342418631
# Average red per pixel in underground: 76.19218047730926
# Average blue per pixel in street: 41.438787223458846
# Average green per pixel in street: 50.28455821909145
# Average red per pixel in street: 57.87510222750569

def main():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--input_path', default=None, type=str)
    parser.add_argument('--remove_whiteout', default=False, type=bool)  # Remove all white level transition images
    parser.add_argument('--whiteout_threshold', default=60, type=float)  # Blue value greater than threshold is an all white image
    parser.add_argument('--resize', default=False, type=bool)
    parser.add_argument('--dimensions', default=None, type=int, nargs=2)
    parser.add_argument('--grayscale', default=False, type=bool)
    parser.add_argument('--get_street', default=False, type=bool)
    parser.add_argument('--get_subway', default=False, type=bool)
    parser.add_argument('--subway_threshold', default=67.6, type=float)  # Red value greater than threshold is subway, below is street
    parser.add_argument('--output_path', default=None, type=str)
    parser.add_argument('--sample_images', default=-1, type=int)
    args = parser.parse_args()

    if args.input_path is None or not os.path.exists(args.input_path):
        print('Provide valid image directory path')
        sys.exit()
    if args.resize == True and args.dimensions == None:
        print('Provide dimensions for new image size when resizing images')
        sys.exit()

    if args.input_path[-1] != '/': args.input_path += '/'
    if args.output_path is None:
        output_path = args.input_path
    elif args.output_path[-1] != '/':
        output_path += '/'

    image_paths = sorted(glob.glob(args.input_path + '*'))
    images = [cv2.imread(image_path) for image_path in image_paths]

    whiteout_images = []
    if args.remove_whiteout:
        images, whiteout_images = filter_whiteout(images, args.whiteout_threshold)

    all_images = {}
    if args.get_subway or args.get_street:
        street_images, subway_images = split_subway_street(images, args.subway_threshold)
        images = np.append(street_images, subway_images)
        if len(street_images) > 0:
            if args.sample_images > len(street_images):
                print('Street image sample size greater than available images. ' + len(street_images) + ' images available')
            elif args.sample_images < len(street_images) and args.sample_images > 0:
                street_images = np.random.choice(street_images, size=args.sample_images, replace=False)
            all_images['street'] = street_images
        if len(subway_images) > 0:
            if args.sample_images > len(subway_images):
                print('Subway image sample size greater than available images. ' + len(subway_images) + ' images available')
            elif args.sample_images < len(subway_images) and args.sample_images > 0:
                subway_images = np.random.choice(subway_images, size=args.sample_images, replace=False)
            all_images['subway'] = subway_images
    else:
        if args.sample_images > len(images):
            print('Image sample size greater than available images. ' + len(images) + ' images available')
        elif args.sample_images < len(images) and args.sample_images > 0:
            images = np.random.choice(images, size=args.sample_images, replace=False)
        all_images['all_images'] = images

    for image_type, images in all_images.items():
        if args.resize:
            images = resize_images(images, args.dimensions)

        if args.grayscale:
            images = graysale_images(images)

        path = output_path + image_type + '/'
        if not os.path.exists(path):
            os.makedirs(path)
        for i, image in enumerate(images):
            cv2.imwrite(path + str(i) + '.png', image)


if __name__ == main():
    main()